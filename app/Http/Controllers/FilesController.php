<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FilesController extends Controller
{
    public function show(){
        $file= storage_path()."\app\GraceHooper.pdf";
        $headers = array(
            'Content-Type: application/pdf',
        );

        return response()->download(storage_path("app\public\Enero21.pdf"));
    }

    public function create(Request $request){
        $path = $request->file('photo')->store('testing');
        return response()->json(['path' => $path ] , 200);
    }


}
